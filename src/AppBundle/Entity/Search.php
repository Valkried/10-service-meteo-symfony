<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Search
 *
 * @ORM\Table(name="search")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SearchRepository")
 */
class Search
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     * @Assert\NotNull(message="Vous devez entrer une adresse")
     * @Assert\NotBlank
     * @Assert\Length(min=1, max=255)
     */
    private $address;

    /**
     * @var Search
     *
     * @ORM\OneToMany(targetEntity="Weather", mappedBy="city")
     */
    private $weatherHistories;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->weatherHistories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Search
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add weatherHistory
     *
     * @param \AppBundle\Entity\Weather $weatherHistory
     *
     * @return Search
     */
    public function addWeatherHistory(\AppBundle\Entity\Weather $weatherHistory)
    {
        $this->weatherHistories[] = $weatherHistory;

        return $this;
    }

    /**
     * Remove weatherHistory
     *
     * @param \AppBundle\Entity\Weather $weatherHistory
     */
    public function removeWeatherHistory(\AppBundle\Entity\Weather $weatherHistory)
    {
        $this->weatherHistories->removeElement($weatherHistory);
    }

    /**
     * Get weatherHistories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWeatherHistories()
    {
        return $this->weatherHistories;
    }
}
