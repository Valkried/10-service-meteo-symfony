<?php

namespace AppBundle\Command;

use AppBundle\Service\OpenWeatherService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppRunWeatherHistoryCommand extends Command
{
    private $em;
    private $weatherService;

    public function __construct(EntityManagerInterface $em, OpenWeatherService $weatherService, ?string $name = null)
    {
        $this->em = $em;
        $this->weatherService = $weatherService;
        
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('app:run-weather-history')
            ->setDescription('Run weather history')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cities = $this->em->getRepository('AppBundle:Search')->findAll();

        foreach ($cities as $city) {
            $weather = $this->weatherService->getWeather($city);
            $weather->setCity($city);
            $this->em->persist($weather);
        }

        $this->em->flush();

        $output->writeln('Weather successfully generated!');
    }

}
