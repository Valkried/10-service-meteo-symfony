<?php

namespace AppBundle\Service;

class TemperatureConverterService
{
    public function kelvinToCelsius($kelvin)
    {
        return $kelvin - 273.15;
    }
}