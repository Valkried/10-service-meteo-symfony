<?php

namespace AppBundle\Service;

use AppBundle\Entity\Search;
use AppBundle\Entity\Weather;

class OpenWeatherService
{
    private $tempConverter;

    const IMG = [
        'Thunderstorm' => 'thunderstorm.png',
        'Drizzle' => 'drizzle.png',
        'Rain' => 'rain.png',
        'Snow' => 'snow.png',
        'Atmosphere' => 'atmosphere.png',
        'Clear' => 'clear.png',
        'Clouds' => 'clouds.png',
        'Extreme' => 'extreme.png',
        'Additional' => 'additional.png',
    ];

    public function __construct(TemperatureConverterService $tempConverter)
    {
        $this->tempConverter = $tempConverter;
    }

    private function getData($address)
    {
        $url = 'http://api.openweathermap.org/data/2.5/weather?q='.$address.'&APPID=b55a1b540849b52bbcb6784d9ff4307b';
        try {
            $data = json_decode(file_get_contents($url));
        } catch (\Exception $e) {
            return false;
        }

        return $data;
    }

    public function getWeather(Search $search)
    {
        $data = $this->getData(rawurlencode($search->getAddress()));

        if (!$data) {
            return false;
        }

        $search->setAddress($data->name);

        $temperature = $this->tempConverter->kelvinToCelsius($data->main->temp);

        $weather = new Weather();
        $weather->setType($data->weather[0]->main);

        if (!in_array($data->weather[0]->main, self::IMG)) {
            $weather->setImg('additional.png');
        } else {
            $weather->setImg(self::IMG[$weather->getType()]);
        }

        $weather->setTemperature($temperature);
        $weather->setCity($search);

        return $weather;
    }
}