<?php

namespace AppBundle\Controller;

use AppBundle\Form\SearchType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $searchForm = $this->createForm(SearchType::class);
        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $search = $searchForm->getData();
            $weather = $this->get('meteo')->getWeather($search);

            if (!$em->getRepository('AppBundle:Search')->findOneByAddress($search->getAddress())) {
                if ($weather) {
                    $em->persist($search);
                    $em->flush();
                }
            } else {
                $search = $em->getRepository('AppBundle:Search')->findOneByAddress($search->getAddress());
            }

            return $this->render('default/index.html.twig', [
                'searchForm' => $searchForm->createView(),
                'weather' => $weather,
                'search' => $search,
            ]);
        }

        return $this->render('default/index.html.twig', [
            'searchForm' => $searchForm->createView(),
        ]);
    }
}
